Name Generator

Generates a random name.
User enters the desired length as well as whether certain letters
shall be consonants, vowels, or completely random.
The program then generates 20 names, based on the parameters.
:)