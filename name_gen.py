# Generates a name based off user entered parameters

import string
import random

vowels = ['a', 'e', 'i', 'o', 'u', 'y']
consonants = ['b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'z']


def ask_input():
    """
    Asks user for input to formulate a name
    :return: a list of strings representing their selections
    """
    letters = []
    num = int(input("\nHow many letters long is the name? "))
    for i in range(num):
        print("\nEnter 'v' for vowel, 'c' for consonant, or 'r' for random letter.")
        letter = input("Letter " + str(i + 1) + " : ")
        letters.append(letter)
    return letters


def generate_name(letters):
    """
    Generates name based on letter template provided
    :param letters: a list of strings(ascii characters) that the user provided
    :return: a generated string of len 5, based off input
    """
    all_letters = string.ascii_lowercase
    name = ''
    for letter in letters:
        if letter == 'v':
            name = name + random.choice(vowels)
        elif letter == 'c':
            name = name + random.choice(consonants)
        elif letter == 'r':
            name = name + random.choice(all_letters)
        else:
            name = name + letter
    return name


flag = '2'

while flag == '1' or flag == '2':
    if flag == '2':
        template = ask_input()
    print('')
    for i in range(20):
        print(generate_name(template).title())
    flag = input("\nPress '1' to generate more names. Press '2' to start over."
                 "\nOtherwise, press 'q' to quit: ")

print("\nThanks for using Seyfire's Name Generator!")
